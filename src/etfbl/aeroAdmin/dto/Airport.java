package etfbl.aeroAdmin.dto;

public class Airport {
	private int id;
	private String countryName;
	private String cityName;
	public Airport(String countryName, String cityName) {
		super();
		this.countryName = countryName;
		this.cityName = cityName;
	}
	
	public Airport(int id, String countryName, String cityName) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.cityName = cityName;
	}
	
	public Airport() {
		
	}

	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public boolean hasId() {
		if(this.id != 0)
			return true;
		return false;
	}
	
	
}
