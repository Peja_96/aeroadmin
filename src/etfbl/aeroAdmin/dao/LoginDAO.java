package etfbl.aeroAdmin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import etfbl.aeroAdmin.DB.DBConnection;

public class LoginDAO {
	public static boolean validate(String user, String password) {

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("Select username, password from admin where username = ? and password = ?");
			ps.setString(1, user);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}
		} catch (SQLException ex) {
			return false;
		}
		return false;
	}
	
	public static HashMap<String,Integer> getLogCount(){
		
		HashMap<String, Integer> map = new HashMap<>();
		String dt = new Date().toString();  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -30);  // number of days to add
        dt = sdf.format(c.getTime());
		java.sql.Date sqlDate = new java.sql.Date(c.getTime().getTime());
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("Select * from logged where date >= ?");
			ps.setDate(1, sqlDate);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				map.put(rs.getDate("date").toString(),rs.getInt("counter"));
			}
		} catch (SQLException ex) {
			
		}
		return map;
	}
}
