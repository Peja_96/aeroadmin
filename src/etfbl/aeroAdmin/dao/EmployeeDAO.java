package etfbl.aeroAdmin.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import etfbl.aeroAdmin.DB.DBConnection;
import etfbl.aeroAdmin.dto.Employee;

public class EmployeeDAO {
	
	public static List<Employee> getAllEmployees() {
		List<Employee> employees = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from employee");
			while (resultSet.next()) {
			
				Integer id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String username = resultSet.getString("username");
				String password = resultSet.getString("password");
				
				employees.add(new Employee(id,name, surname, username, password));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return employees;
	}
	
	public static void addEmployee(Employee emp) {
		Integer id = 0;
		try {
			String sql = "insert into employee values(null, ?, ?, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, emp.getName());
			statement.setString(2, emp.getSurname());
			statement.setString(3, emp.getUsername());
			statement.setString(4, emp.getPassword());
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next())
				id = resultSet.getInt(1);
			emp.setId(id);
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean removeEmployee(int id) {
		try {
			String sql = "delete from employee where id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean editEmployee(Employee emp) {
		try {
			String sql = "update Employee set name = ?, surname = ?, username = ?, password = ? where id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setString(1, emp.getName());
			statement.setString(2, emp.getSurname());
			statement.setString(3, emp.getUsername());
			statement.setString(4, emp.getPassword());
			statement.setInt(5, emp.getId());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		
	}
}
