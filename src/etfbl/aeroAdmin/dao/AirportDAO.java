package etfbl.aeroAdmin.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import etfbl.aeroAdmin.DB.DBConnection;
import etfbl.aeroAdmin.dto.Airport;
import etfbl.aeroAdmin.dto.User;


public class AirportDAO {
	public static List<Airport> getAllFlights() {
		List<Airport> airports = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from airport");
			while (resultSet.next()) {
				
				int id = resultSet.getInt("id");
				String countryName = resultSet.getString("country_name");
				String cityName = resultSet.getString("city_name");
				
				airports.add(new Airport(id,countryName, cityName));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airports;
	}
	
	public static boolean removeAirport(int id) {
		try {
			String sql = "delete from airport where id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean editAirport(Airport airport) {
		try {
			String sql = "update airport set country_name = ?, city_name = ? where id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setString(1, airport.getCountryName());
			statement.setString(2, airport.getCityName());
			statement.setInt(3, airport.getId());
			statement.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean addAirport(Airport airport) {
		try {
			int id = 0;
			String sql = "insert into airport values(null, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, airport.getCountryName());
			statement.setString(2, airport.getCityName());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next())
				id = resultSet.getInt(1);
			airport.setId(id);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
