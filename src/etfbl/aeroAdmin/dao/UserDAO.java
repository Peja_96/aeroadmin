package etfbl.aeroAdmin.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


import etfbl.aeroAdmin.DB.DBConnection;
import etfbl.aeroAdmin.dto.User;

public class UserDAO {
	
	public static List<User> getAllUsers() {
		List<User> users = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from user inner join person where person_id = id");
			while (resultSet.next()) {
			
				Integer id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String username = resultSet.getString("username");
				String password = resultSet.getString("password");
				String email = resultSet.getString("email");
				String userType = resultSet.getString("user_type");
				String country = resultSet.getString("country");
				String address = resultSet.getString("adresa");
				
				users.add(new User(id,name, surname, username, password, email, userType, country, address));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return users;
	}
	
	public static boolean editUser(User user) {
		try {
			String sql = "update person set name = ?, surname = ?, username = ?, password = ? where id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getUsername());
			statement.setString(4, user.getPassword());
			statement.setInt(5, user.getId());
			statement.executeUpdate();
			
			sql = "update user set adresa = ?, user_type = ?, email = ?, country = ? where person_id = ?";
			statement = DBConnection.getConnection().prepareStatement(sql);
			
			statement.setString(1, user.getAddress());
			statement.setString(2, user.getUserType());
			statement.setString(3, user.getEmail());
			statement.setString(4, user.getCountry());
			statement.setInt(5, user.getId());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		
	}

	public static boolean addUser(User user) {
		Integer id = 0;
		try {
			String sql = "insert into person values(null, ?, ?, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getUsername());
			statement.setString(4, user.getPassword());
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next())
				id = resultSet.getInt(1);
			user.setId(id);
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		try {
			String sql = "insert into user values(?, ?, ?, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.setString(2, user.getAddress());
			statement.setString(3, user.getUserType());
			statement.setString(4, user.getEmail());
			statement.setString(5, user.getCountry());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public static boolean removeUser(int id) {
		try {
			String sql = "delete from user where person_id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
			sql = "delete from person where id = ?";
			statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

}
