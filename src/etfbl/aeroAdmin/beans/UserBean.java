package etfbl.aeroAdmin.beans;

import java.io.Serializable;
import java.util.List;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import etfbl.aeroAdmin.dao.UserDAO;
import etfbl.aeroAdmin.dto.User;

@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

	private static final long serialVersionUID = 3354127573757065763L;

	private List<User> users;

    private User selectedUser;

    public void init() {
        this.users = UserDAO.getAllUsers();
    }

    public List<User> getUsers() {
        return users;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }


    public void openNew() {
        this.selectedUser = new User();
    }

    public String deleteUser(User user) {
    	UserDAO.removeUser(user.getId());
        this.users.remove(user);
        return null;
    }
    
    public String addRow() {
    	this.users.add(new User());
    	return null;
    }
    
    public void onRowEdit(RowEditEvent<User> event) {
    	if(event.getObject().hasId())
    		UserDAO.editUser(event.getObject());
    	else
    		UserDAO.addUser(event.getObject());
        FacesMessage msg = new FacesMessage("Product Edited", String.valueOf(event.getObject().getName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent<User> event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(event.getObject().getName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
