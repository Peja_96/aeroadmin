package etfbl.aeroAdmin.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import etfbl.aeroAdmin.dao.AirportDAO;
import etfbl.aeroAdmin.dto.Airport;

@ManagedBean
@SessionScoped
public class AirportBean implements Serializable {

	private static final long serialVersionUID = -9050717236445446765L;

	private List<Airport> airports;

    private Airport selectedAirport;

    public void init() {
        this.airports = AirportDAO.getAllFlights();
    }

    public List<Airport> getAirports() {
        return airports;
    }

    public Airport getSelectedAirport() {
        return selectedAirport;
    }

    public void setSelectedUser(Airport selectedAirport) {
        this.selectedAirport = selectedAirport;
    }


    public String deleteAirport(Airport airport) {
    	AirportDAO.removeAirport(airport.getId());
        this.airports.remove(airport);
        return null;
    }
    
    public String addRow() {
    	this.airports.add(new Airport());
    	return null;
    }
    
    public void onRowEdit(RowEditEvent<Airport> event) {
    	if(event.getObject().hasId())
    		AirportDAO.editAirport(event.getObject());
    	else
    		AirportDAO.addAirport(event.getObject());
        FacesMessage msg = new FacesMessage("Airport Edited", String.valueOf(event.getObject().getCityName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent<Airport> event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(event.getObject().getCityName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
