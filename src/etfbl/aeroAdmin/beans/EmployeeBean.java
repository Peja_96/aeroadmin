package etfbl.aeroAdmin.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import etfbl.aeroAdmin.dao.EmployeeDAO;
import etfbl.aeroAdmin.dto.Employee;

@ManagedBean
@SessionScoped
public class EmployeeBean implements Serializable{

	private static final long serialVersionUID = 1171267392798782613L;
	
	private List<Employee> employees;

    private Employee selectedEmployee;

    public void init() {
        this.employees = EmployeeDAO.getAllEmployees();
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getSelectedEmployee() {
        return selectedEmployee;
    }

    public void setSelectedUser(Employee selectedUser) {
        this.selectedEmployee = selectedUser;
    }


    public void openNew() {
        this.selectedEmployee = new Employee();
    }

    public String deleteEmployee(Employee user) {
    	EmployeeDAO.removeEmployee(user.getId());
        this.employees.remove(user);
        return null;
    }
    
    public String addRow() {
    	this.employees.add(new Employee());
    	return null;
    }
    
    public void onRowEdit(RowEditEvent<Employee> event) {
    	if(event.getObject().hasId())
    		EmployeeDAO.editEmployee(event.getObject());
    	else
    		EmployeeDAO.addEmployee(event.getObject());
        FacesMessage msg = new FacesMessage("Employee Edited", String.valueOf(event.getObject().getName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent<Employee> event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(event.getObject().getName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
